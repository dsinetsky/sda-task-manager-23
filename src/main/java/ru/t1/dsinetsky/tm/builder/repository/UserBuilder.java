package ru.t1.dsinetsky.tm.builder.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.model.User;

public final class UserBuilder {

    @NotNull
    final User user = new User();

    private UserBuilder() {
    }

    @NotNull
    public static UserBuilder create() {
        return new UserBuilder();
    }

    @NotNull
    public UserBuilder login(@NotNull final String login) {
        user.setLogin(login);
        return this;
    }

    @NotNull
    public UserBuilder email(@Nullable final String email) {
        user.setEmail(email);
        return this;
    }

    @NotNull
    public UserBuilder firstName(@Nullable final String firstName) {
        user.setFirstName(firstName);
        return this;
    }

    @NotNull
    public UserBuilder lastName(@Nullable final String lastName) {
        user.setLastName(lastName);
        return this;
    }

    @NotNull
    public UserBuilder middleName(@Nullable final String middleName) {
        user.setMiddleName(middleName);
        return this;
    }

    @NotNull
    public UserBuilder password(@NotNull final String password) {
        user.setPasswordHash(password);
        return this;
    }

    @NotNull
    public UserBuilder role(@NotNull final Role role) {
        user.setRole(role);
        return this;
    }

    @NotNull
    public User toUser() {
        return user;
    }

}
