package ru.t1.dsinetsky.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class InvalidArgumentException extends GeneralSystemException {

    public InvalidArgumentException() {
        super("Invalid argument! Use argument \"-h\" for list of arguments!");
    }

    public InvalidArgumentException(@NotNull final String message) {
        super("Argument " + message + " is not supported! Use argument \"-h\" for list of arguments!");
    }
}
