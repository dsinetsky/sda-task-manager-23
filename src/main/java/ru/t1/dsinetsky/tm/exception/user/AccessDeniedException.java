package ru.t1.dsinetsky.tm.exception.user;

public final class AccessDeniedException extends GeneralUserException {

    public AccessDeniedException() {
        super("Access denied! Please contact your system administrator.");
    }

}
