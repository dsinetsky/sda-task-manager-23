package ru.t1.dsinetsky.tm.exception.system;

public final class InvalidValueException extends GeneralSystemException {

    public InvalidValueException() {
        super("Value is not a number!");
    }

}
