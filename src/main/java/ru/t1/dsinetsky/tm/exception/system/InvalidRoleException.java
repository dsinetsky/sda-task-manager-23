package ru.t1.dsinetsky.tm.exception.system;

public final class InvalidRoleException extends GeneralSystemException {

    public InvalidRoleException() {
        super("Invalid user role!");
    }

}
