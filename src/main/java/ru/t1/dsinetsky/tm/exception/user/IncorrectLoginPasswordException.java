package ru.t1.dsinetsky.tm.exception.user;

public final class IncorrectLoginPasswordException extends GeneralUserException {

    public IncorrectLoginPasswordException() {
        super("Incorrect user login or password!");
    }

}
