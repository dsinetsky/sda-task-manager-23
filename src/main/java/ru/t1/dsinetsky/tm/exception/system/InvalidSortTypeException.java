package ru.t1.dsinetsky.tm.exception.system;

public final class InvalidSortTypeException extends GeneralSystemException {

    public InvalidSortTypeException() {
        super("Invalid sort type!");
    }

}
