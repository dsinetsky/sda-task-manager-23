package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_TASK_CLEAR;

    @NotNull
    public static final String DESCRIPTION = "Clear all tasks";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter name of new task:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getTaskService().create(userId, name, description);
        System.out.println("Task successfully created!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
