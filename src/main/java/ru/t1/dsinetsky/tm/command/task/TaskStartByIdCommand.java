package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_TASK_START_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Starts task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().changeStatusById(userId, id, Status.IN_PROGRESS);
        showTask(task);
        System.out.println("Task successfully started!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
