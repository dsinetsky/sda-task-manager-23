package ru.t1.dsinetsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @Override
    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
        @Nullable final String commandName = command.getName();
        if (!commandName.isEmpty()) mapByName.put(commandName, command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String commandName) {
        return mapByName.get(commandName);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        return mapByArgument.get(argument);
    }

}
