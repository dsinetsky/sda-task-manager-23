package ru.t1.dsinetsky.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.api.service.*;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.command.project.*;
import ru.t1.dsinetsky.tm.command.system.*;
import ru.t1.dsinetsky.tm.command.task.*;
import ru.t1.dsinetsky.tm.command.user.*;
import ru.t1.dsinetsky.tm.command.user.admin.*;
import ru.t1.dsinetsky.tm.constant.VersionConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.system.InvalidArgumentException;
import ru.t1.dsinetsky.tm.exception.system.InvalidCommandException;
import ru.t1.dsinetsky.tm.repository.CommandRepository;
import ru.t1.dsinetsky.tm.repository.ProjectRepository;
import ru.t1.dsinetsky.tm.repository.TaskRepository;
import ru.t1.dsinetsky.tm.repository.UserRepository;
import ru.t1.dsinetsky.tm.service.*;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @NotNull
    @Getter
    private final ITestCreateService testCreateService = new TestCreateService(projectService, taskService, userService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService);

    {
        //register system commands
        registry(new HelpDisplayCommand());
        registry(new ProjectHelpDisplayCommand());
        registry(new TaskHelpDisplayCommand());
        registry(new SystemHelpDisplayCommand());
        registry(new UserHelpDisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());
        registry(new CommandListCommand());
        registry(new ArgumentListCommand());
        registry(new ExitCommand());
        //register project commands
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectListCommand());
        registry(new ProjectFindByIdCommand());
        registry(new ProjectFindByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        //register task commands
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskListCommand());
        registry(new TaskListByProjectCommand());
        registry(new TaskFindByIdCommand());
        registry(new TaskFindByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new BindTaskToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        //register user commands
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserViewProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new CurrentUserUpdateCommand());
        registry(new CurrentUserUpdateEmailCommand());
        //register admin commands.
        registry(new UserCreateCommand());
        registry(new UserUpdateEmailByIdCommand());
        registry(new UserUpdateEmailByLoginCommand());
        registry(new UserUpdateNameByIdCommand());
        registry(new UserUpdateNameByLoginCommand());
        registry(new UserRemoveCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveAllCommand());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void terminalRun(@Nullable final String command) throws GeneralException {
        if (command == null) throw new InvalidCommandException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void argumentRun(@Nullable final String arg) throws GeneralException {
        if (arg == null) throw new InvalidArgumentException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        abstractCommand.execute();
    }

    private boolean argRun(@NotNull final String[] args) throws GeneralException {
        if (args.length < 1) {
            return false;
        }
        @NotNull final String param = args[0];
        argumentRun(param);
        return true;
    }

    private void exitApp() {
        System.exit(0);
    }

    private void initLogger() {
        loggerService.info("Welcome to the task-manager_v." + VersionConst.VERSION + "!\nType \"help\" for list of commands");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("Thank you for using task-manager!");
            }
        });
    }

    private void initUser() throws GeneralException {
        testCreateService.createTest();
    }

    public void run(@NotNull final String[] args) {
        try {
            if (argRun(args)) {
                exitApp();
                return;
            }
        } catch (@NotNull final GeneralException e) {
            System.err.println(e.getMessage());
            exitApp();
            return;
        }
        initLogger();
        try {
            initUser();
        } catch (@NotNull final GeneralException e) {
            loggerService.error(e);
        }
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command:");
                @Nullable final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                terminalRun(command);
            } catch (@NotNull final GeneralException e) {
                loggerService.error(e);
            }
        }
    }

}
